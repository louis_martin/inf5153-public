import groovy.util.logging.Slf4j

@Slf4j
class Bidule {

    def saluer() {
        log.trace 'Trace'
        log.debug 'Debug'
        log.info 'Information'
        log.warn 'Avertissement'
        log.error 'Erreur'
    }

}
