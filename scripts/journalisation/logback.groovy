import ch.qos.logback.classic.filter.ThresholdFilter

appender('CONSOLE', ConsoleAppender) {
    filter(ThresholdFilter) {
        level = WARN
    }
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n"
    }
}

appender('FICHIER', FileAppender) {
    file = 'journalisation.log'
    encoder(PatternLayoutEncoder) {
        pattern = "%date %-5level [%thread] %logger{10} [%file:%line] %msg%n"
    }
}

//logger('Principal', OFF)
//logger('Bidule', INFO)

root(TRACE, ['CONSOLE', 'FICHIER'])
