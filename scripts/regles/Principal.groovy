@Grapes( [
    @Grab('org.easyrules:easyrules-core:2.3.0'),
] )

import static org.easyrules.core.RulesEngineBuilder.aNewRulesEngine

def dossierEtudiant = [
    coursReussis: [ 'INF1120', 'INF2120' ],
    // coursDemandes: [ 'INF1130', 'INF2170' ],
    coursDemandes: [ 'INF1130', 'INF2170', 'INF3172' ],
    messages: [],
]

def reglePrerequis = new ReglePrerequis()

def moteur = aNewRulesEngine().withSilentMode(true).build()
// moteur.registerRule new RegleSimple()
moteur.registerRule reglePrerequis

reglePrerequis.dossierEtudiant = dossierEtudiant
moteur.fireRules()
println dossierEtudiant.messages
