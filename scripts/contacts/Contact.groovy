import groovy.json.*

class Contact {

    int id // réservé pour la BD
    int jeton // réservé pour la BD
    String numero
    private interne = [:]

	String getData() { new JsonBuilder(interne) }

	String getPrettyData() { new JsonBuilder(interne).toPrettyString()  }

	def setData(String json) { interne = new JsonSlurper().parseText(json) }

	def getContact() { interne }

	def getPersonne() { contact.personne }

	def getOrganisation() { contact.organisation }

	def getSequenceTri() { (personne ?: organisation).sequenceTri }

	List getAddressIds() { contact.adresse*.id }

	String getNomPourListe() { personne ? nomPersonne : organisation.nom }

    String getNomPersonne() { "$personne.nomFamille, $personne.prenom" }

	String getNomPourTri() { sequenceTri ?: nomPourListe }

    String toString() { """\
Contact :
    id : $id
    jeton : $jeton
    numero : $numero
    data : $prettyData
"""
    }

}
