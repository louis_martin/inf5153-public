@Singleton(lazy=true)
class ContactGenerateur {

    final static RANDOM = new Random()

    final static FORMAT_DATE = 'yyyy-MM-dd'
    final static DATE_BASE = Date.parse(FORMAT_DATE, '1950-01-01')
    final static NOMBRE_JOURS_ECOULES = new Date().minus(DATE_BASE)

    final static PRENOMS = [
        'Albert',
        'Béatrice',
        'Charles',
        'Diane',
        'Éric',
        'Fanny',
        'Georges',
        'Henriette',
        'Isidor',
        'Jeannette',
        'Karl',
        'Louise',
        'Maurice',
        'Nicole',
        'Oscar',
        'Patricia',
        'Quentin',
        'Rita',
        'Serge',
        'Thérèse',
        'Ulric',
        'Vivianne',
        'Wilfrid',
        'Xéna',
        'Yvon',
        'Zoé',
    ]

    final static NOMS = [
        'Asselin',
        'Bernardin',
        'Champagne',
        'Deneault',
        'Elliot',
        'Falardeau',
        'Gagnon',
        'Harris',
        'Imbeault',
        'Jalbert',
        'Kennedy',
        'Loiselle',
        'Martin',
        'Nadeau',
        'Ostiguy',
        'Paquin',
        'Quenneville',
        'Richmond',
        'Savoie',
        'Therrien',
        'Underwood',
        'Villeneuve',
        'White',
        'Xolan',
        'Young',
        'Zeron',
    ]

    final static PREFIXES = [
        'Docteur',
        'Honorable',
        'Maitre',
        'Professeur',
	]

    final static SUFFIXES = [
        'MBA',
        'Ph. D.',
        'c.r.',
        'LL. B.',
        'avocat',
        'notaire',
    ]

    final static SEXES = [
        '0',
        '1',
        '2',
        '9',
    ]

    final static TYPES = [
        'type un',
        'type deux',
        'type trois',
        'type quatre',
        'type cinq',
        'type six',
        'type sept',
        'type huit',
        'type neuf',
        'type dix',
        'type onze',
        'type douze',
        'type treize',
        'type quatorze',
        'type quinze',
        'type seize',
        'type dix-sept',
        'type dix-huit',
        'type dix-neuf',
        'type vingt',
    ]

    final static RUES = [
        'rue Notre-Dame',
        'boulevard Saint-Jean',
        'rue Prince-Arthur',
        'rue Ontario',
        'avenue Victoria',
        'avenue des Pins',
        'rue Chabot',
        'boulevard Métropolitain',
        '12ième avenue',
        'boulevard Lasalle',
    ]

    final static VILLES = [
        'Saint-Jean',
        'Montréal',
        'Rimouski',
        'Québec',
        'Mont-Tremblant',
        'Sherbrooke',
        'Rigaud',
        'Thetford Mines',
        'Asbestos',
        'Bourcherville',
    ]

    final static PROVINCES = [
        'NB',
        'ON',
        'QC',
    ]

    final static PROTOCOLES = [
        'twitter',
        'facebook',
        'linkedin',
        'skype',
        'google',
    ]

    final static URLS = [
        'http://site.un.org/',
        'http://site.deux.org/',
        'http://site.trois.org/',
        'http://site.quatre.org/',
        'http://site.cinq.org/',
        'http://site.six.org/',
        'http://site.sept.org/',
        'http://site.huit.org/',
        'http://site.neuf.org/',
        'http://site.dix.org/',
    ]

    // Crée une liste de paragraphes de type lorem ipsum
    // à partir du contenu d'un fichier généré via le site http://www.lipsum.com/
    final static LOREM_IPSUM = new File('lorem-ipsum.txt').text.tokenize("\n\n")

    Contact genererContact() {
        new Contact(numero: genererNumero(), interne: genererData())
    }

    def genererNumero() { chaineNumerique(9) }

    def genererData() {
        def map = [:]
        map.with {
            auHasard() ? ( personne = genererPersonne() ) : ( organisation = genererOrganisation() )
            langue = genererLangue()
            adresse = genererSerie(1, 3, this.&genererAdresse)
            courriel = genererSerie(1, 3, this.&genererCourriel)
            telephone = genererSerie(1, 3, this.&genererTelephone)
            clavardage = genererSerie(0, 2, this.&genererClavardage)
            site = genererSerie(0, 2, this.&genererSite)
            image = genererSerie(0, 2, this.&genererImage)
            anniversaire = genererSerie(0, 2, this.&genererAnniversaire)
            note = auHasard() ? genererNote() : ''
        }
        map
    }

    def genererPersonne() {
        [   nomFamille: auHasardDans(NOMS),
            prenom: auHasardDans(PRENOMS),
            secondPrenom: auHasard() ? auHasardDans(PRENOMS) : '',
            prefixe: auHasard() ? auHasardDans(PREFIXES) : '',
            suffixe: auHasard() ? auHasardDans(SUFFIXES) : '',
            nomFamilleNaissance: auHasard() ? auHasardDans(NOMS) : '',
            etiquetteNom: '',
            sequenceTri: '',
            sexe: auHasardDans(SEXES),
            dateNaissance: genererDate(),
        ]
    }

    def genererOrganisation() {
        [   typeOrganisation: genererListe(0, 1, TYPES),
            nom: 'Compagnie ' + chaineNumerique(4),
            nomComplementaire: 'Division ' + chaineNumerique(2),
            nomCourt: '',
            etiquetteNom: '',
            sequenceTri: '',
        ]
    }

    def genererLangue() {
        [ 'fr', 'en' ]
    }

    def genererAdresse() {
        [   id: chaineNumerique(8),
            typeAdresse: genererListe(0, 3, TYPES),
            mentionComplementaire: 'Une mention complémentaire',
            rue: chaineNumerique(4) + ' ' + auHasardDans(RUES),
            unite: auHasard() ? chaineNumerique(3) : '',
            casePostale: '',
            ville: auHasardDans(VILLES),
            province: auHasardDans(PROVINCES),
            codePostal: '',
            pays: 'CD',
            etiquetteAdresse: '',
        ]
    }

    def genererCourriel() {
        [   typeCourriel: genererListe(0, 3, TYPES),
            adresseCourriel: chaineNumerique(6) + '@' + auHasardDans(URLS)[7..-2],
        ]
    }

    def genererTelephone() {
        [   typeTelephone: genererListe(0, 3, TYPES),
            numeroTelephone: genererNumeroTelephone(),
        ]
    }

    def genererClavardage() {
        def protocole = auHasardDans(PROTOCOLES)
        [   typeClavardage: genererListe(0, 3, TYPES),
            protocoleClavardage: protocole,
            identifiantClavardage: protocole[0..3] + chaineNumerique(6),
        ]
    }

    def genererSite() {
        [   typeSite: genererListe(0, 3, TYPES),
            url: auHasardDans(URLS),
        ]
    }

    def genererImage() {
        [   typeImage: genererListe(0, 3, TYPES),
            url: auHasardDans(URLS),
        ]
    }

    def genererAnniversaire() {
        [   description: 'Une petite description',
            date: genererDate(),
        ]
    }

    def genererNote() {
        auHasardDans(LOREM_IPSUM)
    }

    def genererSerie(min, max, methode) {
        def nombre = RANDOM.nextInt(max - min + 1) + min
        (0..<nombre).inject([]) { serie, i -> serie << methode() }
    }

    def genererListe(min, max, source) {
        def nombre = RANDOM.nextInt(max - min + 1) + min
        (0..<nombre).inject([]) { liste, i -> liste << auHasardDans(source) }
    }

    def auHasard() {
        RANDOM.nextBoolean()
    }

    def auHasardDans(liste) {
        liste[ RANDOM.nextInt(liste.size()) ]
    }

    def chaineNumerique(length) {
        (0..<length).inject('') { chaine, i -> chaine + RANDOM.nextInt(10) }
    }

    def genererDate() {
        DATE_BASE.plus(RANDOM.nextInt(NOMBRE_JOURS_ECOULES)).format(FORMAT_DATE)
    }

    def genererNumeroTelephone() {
        "(${chaineNumerique(3)}) ${chaineNumerique(3)}-${chaineNumerique(4)}"
    }

}
