
// Voir aussi http://alvinalexander.com/java/jwarehouse/groovy/src/test/groovy/swing/SwingBuilderBindingsTest.groovy.shtml

// Dans le 'bind', il est possible de définir 'validator' et 'converter'

import groovy.beans.Bindable
import groovy.swing.SwingBuilder
import static javax.swing.JFrame.EXIT_ON_CLOSE

class Modele {

  @Bindable String texteA = 'XXX'
  @Bindable String texteB = 'YYY'

  def reset() {
      this.setTexteA( '' ) // et non texteA = ''
      this.setTexteB( '' ) // et non texteB = ''
  }

  String toString() {
      this.getTexteA() + ' ' + this.getTexteB()
  }

}

def modele = new Modele()

new SwingBuilder().edt {
    frame(  title: 'Bindable: couplage fort',
            pack: true,
            locationRelativeTo: null,
            show: true,
            defaultCloseOperation: EXIT_ON_CLOSE ) {
        gridLayout(cols: 2, rows: 0)

        label ' Champ 1 : '
        textField(  columns: 10, id: 'champ1',
                    text: bind( 'texteA', source: modele, mutual: true ) )
        // textField( columns: 10, id: 'champ1', text: bind( source: modele, sourceProperty: 'texteA', mutual: true ) )
        // bean( modele, texteA: bind{ champ1.text } )

        label ' Champ 2 : '
        textField(  columns: 10, id: 'champ2',
                    text: bind( 'texteB', source: modele, mutual: true ) )
        // textField( columns: 10, id: 'champ2', text: bind( source: modele,
        //     sourceProperty: 'texteB', mutual: true ) )
        // bean( modele, texteB: bind{ champ2.text } )

        label ' Contenu du modèle : '
        label( text: bind { "$modele.texteA  $modele.texteB" } )
        //label( text: bind { "${modele.toString()}" } ) // attention pas maj

        label ' Bouton : '
        button( text: 'Bouton',
                enabled: bind { modele.texteA && modele.texteB } )

        label ' Remettre le modèle à blanc : '
        button( text: 'Exécuter',
                actionPerformed: { modele.reset() }  )

    }
}
