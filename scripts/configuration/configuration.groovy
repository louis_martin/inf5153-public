variations {

    production {
        mode = 'production'
    }

    preproduction {
        mode = 'preproduction'
        usage = 'Formation'
    }

    integration {
        mode = 'integration'
        usage = 'Tests d\'intégration'
    }

    test {
        mode = 'test'
        usage = 'Environnement de test'
    }

    developpement {
        mode = 'developpement'
        usage = 'Poste du développeur'
    }

}

autre = 'Autre'

usage = 'Général'
