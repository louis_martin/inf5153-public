@Grapes( [
    @Grab('org.codehaus.groovy:groovy-xmlrpc:0.8'),
    @Grab('ch.qos.logback:logback-classic:1.1.7'),
    @GrabConfig(systemClassLoader=true)
] )

import groovy.util.logging.Slf4j

@Slf4j
class Principal {

    static void main(String[] args) {
        log.trace 'Début'
        def config = Configurateur.chargerConfig()
        log.trace 'Fichier de configuration chargé'
        new ServiceVersement().start(config.serviceVersement.port)
        new ServiceCalendrier().start(config.serviceCalendrier.port)
        new ServiceMarkdown().start(config.serviceMarkdown.port)
        log.trace 'Fin'
    }

}
