import groovy.json.JsonSlurper

import groovy.util.logging.Slf4j

@Slf4j
class ServiceMarkdown extends Service {

    {
        server.genererMarkdown = this.&genererMarkdown
    }

    String genererMarkdown(String json) {
        log.trace 'Début'
        def pret = new JsonSlurper().parseText json
        def resultat = """\
        % Calcul du remboursement d'un prêt
        % Louis Martin -- INF5153 -- UQAM
        % \\today

        ---
        header-includes:
            - \\usepackage{floatrow}
            - \\DeclareFloatFont{normalsize}{\\normalsize}
            - \\floatsetup[table]{font=normalsize}
        ---

        # Paramètres du prêt

        Scénario : **$pret.parametres.scenario**

        Date du prêt : **$pret.parametres.datePret**

        Montant initial du prêt : **${formatDollar(pret.parametres.montantInitial)}**

        Nombre de mensualités : **$pret.parametres.nombrePeriodes**

        Taux mensuel : **${formatPourcent(pret.parametres.tauxPeriodique)}**

        Versement mensuel : **${formatDollar(pret.parametres.versement)}**

        # Calendrier de remboursement

        | **No** | **Date** | **Solde début** | **Versement** | **Intérêt** | **Capital** | **Solde fin** |
        |:---|:----------:|------------:|----------:|----------:|----------:|------------:|
        ${markdownLignes(pret.calendrier)}

        # Totaux

        Total des paiements : **${formatDollar(pret.calendrier.last().totalCumulatif)}**

        Total des intérêts : **${formatDollar(pret.calendrier.last().interetCumulatif)}**

        Total du capital remboursé : **${formatDollar(pret.calendrier.last().capitalCumulatif)}**
        """.stripIndent()
        log.trace 'Fin'
        resultat
    }

    def markdownLignes(calendrier) {
        calendrier.collect { ligne -> ligne.with {
            "|$periode|${formatDate(date)}|${formatDollar(capitalDebut)}" +
            "|${formatDollar(versement)}|${formatDollar(interet)}" +
            "|${formatDollar(capital)}|${formatDollar(capitalFin)}|\n        "
        } }.join()
    }

    def formatDate(date) {
        date.format 'yyyy-MM-dd'
    }

    def formatDollar(montant) {
        def formateur = java.text.NumberFormat.currencyInstance
        formateur.format(montant as BigDecimal)
    }

    def formatPourcent(pourcent) {
        def formateur = new java.text.DecimalFormat('###.##\u00A0%')
        formateur.format(pourcent as BigDecimal)
    }

}
