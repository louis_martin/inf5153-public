@Grab('org.codehaus.groovy:groovy-xmlrpc:0.8')

import groovy.net.xmlrpc.XMLRPCServerProxy
import static groovy.json.JsonOutput.prettyPrint

def config = Configurateur.chargerConfig()
def host = config.serveurHote
def serviceVersement = creerProxy(host,config.serviceVersement.port)
def serviceCalendrier = creerProxy(host,config.serviceCalendrier.port)
def serviceMarkdown = creerProxy(host,config.serviceMarkdown.port)

def creerProxy(host,port)  {
    new XMLRPCServerProxy("http://${host}:${port}")
}

println serviceVersement.id()
println serviceCalendrier.id()
println serviceMarkdown.id()

def fichierIntrant = args ? args[0] : 'parametres.json'
def intrant = new File(fichierIntrant).text

def parametres = serviceVersement.calculerVersement(intrant)
println prettyPrint(parametres)

def calendrier = serviceCalendrier.produireCalendrier(parametres)
println prettyPrint(calendrier)

def markdown = serviceMarkdown.genererMarkdown(calendrier)
println markdown
