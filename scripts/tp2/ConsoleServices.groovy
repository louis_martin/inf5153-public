@Grapes( [
    @Grab('org.codehaus.groovy:groovy-xmlrpc:0.8'),
    @Grab('ch.qos.logback:logback-classic:1.1.7'),
    @GrabConfig(systemClassLoader=true)
] )

import groovy.swing.SwingBuilder
import groovy.util.logging.Slf4j
import java.awt.Font
import javax.swing.BorderFactory
import javax.swing.JOptionPane
import static javax.swing.JFrame.EXIT_ON_CLOSE
import static javax.swing.SwingConstants.CENTER

@Slf4j
class ConsoleServices {

    static void main(String[] args) {
        log.trace 'Début'
        def config = Configurateur.chargerConfig()
        log.trace 'Fichier de configuration chargé'
        def gestion = new GestionServices(
            portVersement: config.serviceVersement.port,
            portCalendrier: config.serviceCalendrier.port,
            portMarkdown: config.serviceMarkdown.port
        )
        construireGUI(gestion)
        addShutdownHook { log.trace 'Fin' }
    }

    static construireGUI(gestion) {

        new SwingBuilder().edt {
            // lookAndFeel 'system' // defaut Mac OS X
            // lookAndFeel 'nimbus'
            // lookAndFeel 'motif'
            // lookAndFeel 'metal'

            def bordureAvecTitre = { titre ->
                BorderFactory.createTitledBorder(titre)
            }

            def setEtatBoutons = {
                boutonVersementOn.enabled = !gestion.versement
                boutonVersementOff.enabled = gestion.versement
                boutonCalendrierOn.enabled = !gestion.calendrier
                boutonCalendrierOff.enabled = gestion.calendrier
                boutonMarkdownOn.enabled = !gestion.markdown
                boutonMarkdownOff.enabled = gestion.markdown
                boutonGlobalOn.enabled = !gestion.versement || !gestion.calendrier || !gestion.markdown
                boutonGlobalOff.enabled = gestion.versement || gestion.calendrier || gestion.markdown
                caseVersement.selected = gestion.versement
                caseCalendrier.selected = gestion.calendrier
                caseMarkdown.selected = gestion.markdown
            }

            def traiter = { closure ->
                try {
                    closure()
                    setEtatBoutons()
                } catch( exception ) {
                    JOptionPane.showMessageDialog(null, exception.message)
                }
            }

            frame(
                title: 'Console des services',
                size: [600, 400],
                locationRelativeTo: null,
                show: true,
                defaultCloseOperation: EXIT_ON_CLOSE
            ) {
                gridLayout( cols: 1, rows: 0 )
                label(
                    id: 'titre',
                    text: 'Console des services',
                    horizontalAlignment: CENTER,
                    font: ['Lucida', Font.BOLD, 24 ]
                )
                panel(
                    id: 'versement',
                    border: bordureAvecTitre('Service de versement')
                ) {
                    flowLayout()
                    label( text: 'Port :')
                    textField(  id: 'portVersement',
                                text: gestion.portVersement,
                                columns: 5,
                                enabled: false )
                    checkBox(   'Service lancé',
                                id: 'caseVersement',
                                enabled: false )
                    button( 'Lancer le service',
                            id: 'boutonVersementOn',
                            actionPerformed: { ev ->
                                traiter { gestion.lancerVersement() }
                            } )
                    button( 'Arrêter le service',
                            id: 'boutonVersementOff',
                            actionPerformed: { ev ->
                                traiter { gestion.arreterVersement() }
                            } )
                }
                panel(
                    id: 'calendrier',
                    border: bordureAvecTitre('Service de calendrier')
                ) {
                    flowLayout()
                    label( text: 'Port :')
                    textField(  id: 'portCalendrier',
                                text: gestion.portCalendrier,
                                columns: 5,
                                enabled: false )
                    checkBox(   'Service lancé',
                                id: 'caseCalendrier',
                                enabled: false )
                    button( 'Lancer le service',
                            id: 'boutonCalendrierOn',
                            actionPerformed: { ev ->
                                traiter { gestion.lancerCalendrier() }
                            } )
                    button( 'Arrêter le service',
                            id: 'boutonCalendrierOff',
                            actionPerformed: { ev ->
                                traiter { gestion.arreterCalendrier() }
                            } )
                }
                panel(
                    id: 'markdown',
                    border: bordureAvecTitre('Service de markdown')
                ) {
                    flowLayout()
                    label( text: 'Port :')
                    textField(  id: 'portMarkdown',
                                text: gestion.portMarkdown,
                                columns: 5,
                                enabled: false )
                    checkBox(   'Service lancé',
                                id: 'caseMarkdown',
                                enabled: false )
                    button( 'Lancer le service',
                            id: 'boutonMarkdownOn',
                            actionPerformed: { ev ->
                                traiter { gestion.lancerMarkdown() }
                            } )
                    button( 'Arrêter le service',
                            id: 'boutonMarkdownOff',
                            actionPerformed: { ev ->
                                traiter { gestion.arreterMarkdown() }
                            } )
                }
                panel(
                    id: 'global',
                    border: bordureAvecTitre('Commandes globales')
                ) {
                    flowLayout()
                    button( 'Lancer les services',
                            id: 'boutonGlobalOn',
                            actionPerformed: { ev ->
                                traiter { gestion.lancerGlobal() }
                            } )
                    button( 'Arrêter les services',
                            id: 'boutonGlobalOff',
                            actionPerformed: { ev ->
                                traiter { gestion.arreterGlobal() }
                            } )
                }
                setEtatBoutons()
            }
        }

    }

}
