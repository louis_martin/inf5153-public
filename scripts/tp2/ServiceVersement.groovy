import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.toJson

import groovy.util.logging.Slf4j

@Slf4j
class ServiceVersement extends Service {

    {
        server.calculerVersement = this.&calculerVersement
    }

    String calculerVersement(String json) {
        log.trace 'Début'
        def parametres = new JsonSlurper().parseText json
        def montant = parametres.montantInitial as Double
        def taux = parametres.tauxPeriodique as Double
        def nombrePeriodes = parametres.nombrePeriodes as int
        def versement = montant * taux / (1 - (1 + taux) ** (- nombrePeriodes))
        parametres.versement = versement.round(2) as String
        def resultat = toJson parametres
        log.trace 'Fin'
        resultat
    }

}
