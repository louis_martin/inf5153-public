import groovy.util.logging.Slf4j

@Slf4j
class GestionServices {

    def versement = false
    def calendrier = false
    def markdown = false

    def portVersement
    def portCalendrier
    def portMarkdown

    def serviceVersement = new ServiceVersement()
    def serviceCalendrier = new ServiceCalendrier()
    def serviceMarkdown = new ServiceMarkdown()

    def lancerVersement() {
        serviceVersement.start(portVersement)
        versement = true
    }

    def arreterVersement() {
        serviceVersement.stop()
        versement = false
    }

    def lancerCalendrier() {
        serviceCalendrier.start(portCalendrier)
        calendrier = true
    }

    def arreterCalendrier() {
        serviceCalendrier.stop()
        calendrier = false
    }

    def lancerMarkdown() {
        serviceMarkdown.start(portMarkdown)
        markdown = true
    }

    def arreterMarkdown() {
        serviceMarkdown.stop()
        markdown = false
    }

    def lancerGlobal() {
        versement ? null : lancerVersement()
        calendrier ? null : lancerCalendrier()
        markdown ? null : lancerMarkdown()
    }

    def arreterGlobal() {
        versement ? arreterVersement() : null
        calendrier ? arreterCalendrier() : null
        markdown ? arreterMarkdown() : null
    }

}
