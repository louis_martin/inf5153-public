// Fichier de configuration des services

// URL du serveur

serveurHote = 'localhost'

// Services

serviceVersement {
    port = 8090
}

serviceCalendrier {
    port = 8091
}

serviceMarkdown {
    port = 8092
}
