import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.toJson
import java.math.RoundingMode

import groovy.util.logging.Slf4j

@Slf4j
class ServiceCalendrier extends Service {

    {
        server.produireCalendrier = this.&produireCalendrier
    }

    String produireCalendrier(String json) {
        log.trace 'Début'
        def parametres = new JsonSlurper().parseText json
        def date = Date.parse 'yyyy-MM-dd', parametres.datePret
        def nombrePeriodes = parametres.nombrePeriodes as int
        def taux = parametres.tauxPeriodique as BigDecimal
        def capitalDebut = parametres.montantInitial as BigDecimal
        def versement = parametres.versement as BigDecimal
        def totalCumulatif = BigDecimal.ZERO
        def interetCumulatif = BigDecimal.ZERO
        def capitalCumulatif = BigDecimal.ZERO
        def calendrier = (1..nombrePeriodes).collect {
            use (groovy.time.TimeCategory) {
                date = date + 1.month
            }
            def interet = (capitalDebut * taux).setScale 2, RoundingMode.HALF_UP
            def capital = versement - interet
            def capitalFin = capitalDebut - capital
            totalCumulatif += versement
            interetCumulatif += interet
            capitalCumulatif += capital
            def ligne = [
                periode: it,
                date: date,
                capitalDebut: capitalDebut,
                versement: versement,
                interet: interet,
                capital: capital,
                capitalFin: capitalFin,
                totalCumulatif: totalCumulatif,
                interetCumulatif: interetCumulatif,
                capitalCumulatif: capitalCumulatif,
            ]
            capitalDebut = capitalFin
            ligne
        }
        def resultat = toJson( ([ parametres: parametres,
                                  calendrier: calendrier ]) )
        log.trace 'Fin'
        resultat
    }

}
