import ch.qos.logback.classic.filter.ThresholdFilter

appender('CONSOLE', ConsoleAppender) {
    filter(ThresholdFilter) {
        level = INFO
    }
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n"
    }
}

appender('FICHIER', FileAppender) {
    file = 'journalisation.log'
    filter(ThresholdFilter) {
        level = TRACE
    }
    encoder(PatternLayoutEncoder) {
        pattern = "%date %-5level [%thread] %logger{10} [%file:%line] %msg%n"
    }
}

// logger('Principal', OFF)
// logger('ServiceVersement', INFO)
// logger('ServiceCalendrier', TRACE)
// logger('ServiceMarkdown', INFO)

root(TRACE, ['CONSOLE', 'FICHIER'])
