import java.net.ServerSocket
import groovy.net.xmlrpc.XMLRPCServer

import groovy.util.logging.Slf4j

@Slf4j
abstract class Service {

    final server = new XMLRPCServer()
    int port = 0

    Service() {
        server.id = this.&id
    }

    String id() {
        this.class.name + ':' + port
    }

    def start(int portCible) {
        if (port) {
            def message = "Service déja lancé -> ${id()}"
            log.error message
            throw new Exception(message)
        }
        port = portCible
        def serverSocket = new ServerSocket(port)
        server.startServer(serverSocket)
        log.info '{} démarré sur le port {}', this.class.name, port
    }

    def stop() {
        server.stopServer()
        log.info '{} arrêté sur le port {}', this.class.name, port
        port = 0
    }

}
