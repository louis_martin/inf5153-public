import groovy.swing.SwingBuilder
import static javax.swing.JFrame.EXIT_ON_CLOSE

def swing = new SwingBuilder()

swing.edt {
    frame( title: 'Exemple simple avec Swing', pack: true, show: true,
           defaultCloseOperation: EXIT_ON_CLOSE ) {
        gridLayout( cols: 1, rows: 3 )
        textField( id: 'texte', columns: 20 )
        label( id: 'label' )
        button( 'Mettre à jour', actionPerformed: { evt ->
            label.text = texte.text
        })
    }
}
