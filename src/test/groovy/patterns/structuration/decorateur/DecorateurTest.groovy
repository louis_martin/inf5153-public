package patterns.structuration.decorateur

import spock.lang.Specification

class DecorateurTest extends Specification {

    def "Café régulier"() {
        given: "Un café régulier"
            def cafe = new CafeRegulier()
        expect: "La description et le prix conséquent"
            cafe.description == "Café régulier"
            cafe.prix == 1.50
    }

    def "Café décaféiné avec crème fouettée"() {
        given: "Un café décaféiné auquel on ajoute de la crème fouettée"
            def cafe = new CafeDecaf()
            cafe = new GarnitureCreme(cafe)
        expect: "La description et le prix conséquent"
            cafe.description == "Café décaféiné avec crème fouettée"
            cafe.prix == 2.00
    }

    def "Café corsé avec chocolat"() {
        given: "Un café corsé auquel on ajoute du chocolat"
            def cafe = new CafeCorse()
            cafe = new GarnitureChocolat(cafe)
        expect: "La description et le prix conséquent"
            cafe.description == "Café corsé avec chocolat"
            cafe.prix == 1.95
    }

    def "Café BlueMountain avec Smarties"() {
        given: "Un café BlueMountain auquel on ajoute des Smarties"
            def cafe = new CafeBlueMountain()
            cafe = new GarnitureSmarties(cafe)
        expect: "La description et le prix conséquent"
            cafe.description == "Café BlueMountain avec Smarties"
            cafe.prix == 2.65
    }

    def "Café régulier avec chocolat et Smarties"() {
        given: "Un café régulier auquel on ajoute du chocolat et des Smarties"
            def cafe = new CafeRegulier()
            cafe = new GarnitureChocolat(cafe)
            cafe = new GarnitureSmarties(cafe)
        expect: "La description et le prix conséquent"
            cafe.description == "Café régulier avec chocolat avec Smarties"
            cafe.prix == 2.10
    }

    def "Café décaféiné avec double crème fouettée"() {
        given: "Un café décaféiné auquel on ajoute deux fois de la crème fouettée"
            def cafe = new CafeDecaf()
            cafe = new GarnitureCreme(cafe)
            cafe = new GarnitureCreme(cafe)
        expect: "La description et le prix conséquent"
            cafe.description == "Café décaféiné avec crème fouettée avec crème fouettée"
            cafe.prix == 2.30
    }

}
