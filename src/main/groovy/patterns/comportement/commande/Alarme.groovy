package patterns.comportement.commande

class Alarme {

    def armer() {
        println 'Alarme armée'
    }

    def desarmer() {
        println 'Alarme désarmée'
    }

}
