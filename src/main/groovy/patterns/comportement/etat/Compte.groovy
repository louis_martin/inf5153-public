package patterns.comportement.etat

class Compte implements CompteTransaction {

    String numero

    BigDecimal solde = 0.00

    CompteEtat etat

    Compte(String numero) {
        this.numero = numero
        setEtatRegulier()
    }

    def deposer(BigDecimal montant) { etat.deposer montant }

    def retirer(BigDecimal montant) { etat.retirer montant }

    def setEtatDeces() { etat = new CompteEtatDeces(this) }

    def setEtatRestreint() { etat = new CompteEtatRestreint(this) }

    def setEtatRegulier() { etat = new CompteEtatRegulier(this) }

    String toString() { """
        Compte numéro : $numero
        État : ${etat.class.simpleName}
        Solde : ${java.text.NumberFormat.currencyInstance.format solde}
        """.stripIndent()
    }

}
