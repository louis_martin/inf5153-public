package patterns.structuration.decorateur

class CafeRegulier implements Cafe {

    {
        description = "Café régulier"
        prix = 1.50
    }

}
