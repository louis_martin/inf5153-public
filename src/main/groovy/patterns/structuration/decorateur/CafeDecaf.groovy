package patterns.structuration.decorateur

class CafeDecaf implements Cafe {

    {
        description = "Café décaféiné"
        prix = 1.70
    }

}
