<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Jour de chance ?</title>
    </head>
    <body>
        <%
            double num = Math.random();
            if (num > 0.95) {
                %>
                <h2>Vous êtes chanceux aujourd'hui !</h2><p>(<%= num %>)</p>
                <%
            } else {
                %>
                <h2>La vie continue...</h2><p>(<%= num %>)</p>
                <%
            }
        %>
        <a href="javascript:history.go(0)"><h3>Essayez à nouveau</h3></a>
    </body>
</html>
