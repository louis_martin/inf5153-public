# LISEZ-MOI

### Pourquoi ce dépôt ?

- Pour contenir quelques exemples de code en [Groovy](http://www.groovy-lang.org/).

## Répertoire `pour-tomcat`

Ce répertoire contient des applications de type _servlet_. Les sous-répertoires doivent être copiés dans le sous-répertoire `webapps` de Tomcat.

Le sous-répertoire `groovy`contient des _groovlets_. Ne pas oublier de mettre le fichier `groovy-all-2.4.7.jar` dans le sous-répertoire `WEB-INF/lib`. Ne pas oublier également de mettre le pilote (_driver_) JDBC dans le répertoire `lib` de Tomcat.

Le sous-répertoire `jsp` contient un exemple de Java Server Page.

## Répertoire `scripts`

Ce répertoire contient des scripts *Groovy* exécutables directement.

- Le sous-répertoire `bindable` contient un exemple relatif à cette approche.

- Le sous-répertoire `configuration` utilise la manière *Groovy* de configurer une application.

- Le sous-répertoire `contacts` contient un exemple de génération de données de test et d'utilisation de PostgreSQL pour stocker des documents sous format JSON.

- Le sous-répertoire `journalisation` contient un exemple de journalisation utilisant les cadriciels suivants :

    - [Logback](http://logback.qos.ch/) en arrière plan
    - Simple Logging Facade for Java [SLF4J](http://www.slf4j.org/) en facade

    Deux approches possibles pour la configuration (XML et *Groovy*) sont présentées.

- Le sous-répertoire `literate` contient un script et un exemple permettant d'approcher le concept de *literate programming* proposé par [Donald Knuth](https://fr.wikipedia.org/wiki/Programmation_lettr%C3%A9e). Le script est dépendant de [Pandoc](http://pandoc.org/) et de [LaTeX](https://www.latex-project.org/).

- Le sous-répertoire `persistance` contient des exemples avec les bases de données suivantes :

    - [HSQLDB](http://hsqldb.org/)
    - [PostgreSQL](https://www.postgresql.org/) (nécessite l'installation préalable de PostgreSQL)
    - [SQLite](https://sqlite.org/)


- Le sous-répertoire `ratpack` contient des exemples simples avec le cadriciel [Ratpack](https://ratpack.io/). Ratpack permet de développer des microservices avec le protocole [HTTP](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol), le patron de conception [REST](https://fr.wikipedia.org/wiki/Representational_state_transfer) utilisant entre autres le format [JSON](https://fr.wikipedia.org/wiki/JavaScript_Object_Notation).

- Le sous-répertoire `regles` contient un exemple d'utilisation du moteur de règles [Easy Rules](http://www.easyrules.org/).

- Le sous-répertoire `swingbuilder` contient un exemple d'une fenêtre simple.

- Le sous-répertoire `tp1` contient une solution possible au TP1.

- Le sous-répertoire `tp2` contient deux solutions possibles au TP2. Une solution est en mode terminal pour lancer les services, une deuxième utilise Swing.

- Le sous-répertoire `xmlrpc` contient un serveur et un client utilisant le protocole XML-RPC pour communiquer.

## Répertoire `src/main/groovy`

Ce répertoire utilise *Gradle*. Ce logiciel doit être
préinstallé et paramétré correctement.

- Le sous-répertoire `patterns` contient des exemples de quelques patrons de conception.

## Répertoire `src/test/groovy`

Ce répertoire associé au précédent contient des tests. Les tests utilisent le cadriciel (*framework*) [Spock](http://spockframework.org/).
